|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/preconfig/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/preconfig/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/preconfig/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/preconfig/commits/develop)

Role Name
=========
An Ansible Role performs base config required to apply other playbooks on
servers under RedHat/CentOS, Debian/Ubuntu and Alpine Linux operating systems.


Requirements
------------
- Ansible 2.9 and higher


Role Variables
--------------
```yaml
    # Python interpreter version to install
    preconfig_python_version: 2
```


Dependencies
------------
None


Example Playbook
----------------
```yaml

- hosts: all
  # might help to avoid error if Python is not installed
  gather_facts: no
  tasks:
    - include_role:
        name: preconfig
      vars:
        preconfig_python_version: 3

```


License
-------
MIT


Author Information
------------------
ITSupportMe, LLC
