---

# The `Dependency` manager. Molecule uses 'galaxy' by default to resolve your role dependencies.
dependency:
  name: galaxy

# The `Driver` provider. Molecule uses 'Docker' by default. Molecule uses the driver to delegate the task of creating
# instances.
driver:
  name: ${DRIVER_NAME:-docker}

# The `Lint` command. Molecule can call external commands to ensure that best practices are encouraged.
lint: |
  set -e
  yamllint .
  ansible-lint --force-color --exclude=meta --exclude=molecule
  flake8 --ignore=E501
# see how to ignore some flake8 errors:
#   * https://flake8.pycqa.org/en/3.1.1/user/ignoring-errors.html

.common_instance_opts: &common_instance_opts
  docker_host: ${DOCKER_HOST:-tcp://127.0.0.1:2375}
  override_command: False
  security_opts:
    - seccomp=unconfined
    - apparmor=unconfined
  volumes:
    - /sys/fs/cgroup:/sys/fs/cgroup:ro
  tmpfs:
    - /tmp
    - /run
    - /run/lock
  capabilities:
    - SYS_ADMIN
  privileged: False

# The `Platforms` definitions. Molecule relies on this to know which instances to create, name and to which group each
# instance belongs. If you need to test your role against multiple popular distributions (CentOS, Fedora, Debian), you
# can specify that in this section.
platforms:
  - name: debian-10
    image: gitlab.itsupportme.by:5005/docker-images/ansible-debian10:latest
    pre_build_image: True
    <<: *common_instance_opts
  - name: ubuntu-18
    image: gitlab.itsupportme.by:5005/docker-images/ansible-ubuntu1804:latest
    pre_build_image: True
    <<: *common_instance_opts
  - name: ubuntu-20
    image: gitlab.itsupportme.by:5005/docker-images/ansible-ubuntu2004:latest
    pre_build_image: True
    <<: *common_instance_opts
  - name: centos-7
    image: gitlab.itsupportme.by:5005/docker-images/ansible-centos7:latest
    pre_build_image: True
    <<: *common_instance_opts

# The `Provisioner`. Molecule only provides an 'Ansible' provisioner. Ansible manages the life cycle of the instance
# based on this configuration.
provisioner:
  name: ansible
  log: false
  playbooks:
    converge: ../_shared/playbooks/converge.yml
  inventory:
    group_vars:
      all:
        ansible_user: ansible-user

        # variables for current scenario
        preconfig_python_version: 3

# The `Verifier` framework. Molecule uses 'Ansible' by default to provide a way to write specific state checking tests
# (such as deployment smoke tests) on the target instance.
verifier:
  name: testinfra
  # by default 'Testinfra' runs all 'test_*.py' from 'tests' directory in current scenario
  options:
    s: true
  #directory: ../_shared/tests/
