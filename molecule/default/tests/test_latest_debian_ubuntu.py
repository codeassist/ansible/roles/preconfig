import pytest

testinfra_hosts = [
    "debian-10",
    "ubuntu-20"
]


@pytest.mark.parametrize("name", [
    "python"
])
def test_packages(host, name):
    result = host.ansible('debug', 'var=preconfig_python_version')

    pkg = host.package(name + str(result['preconfig_python_version']))
    assert pkg.is_installed
    assert pkg.version.startswith(str(result['preconfig_python_version']))
