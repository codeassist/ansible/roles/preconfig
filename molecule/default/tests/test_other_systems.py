import pytest

testinfra_hosts = [
    "centos-7",
    "ubuntu-18"
]


@pytest.mark.parametrize("name", [
    "python"
])
def test_packages(host, name):
    result = host.ansible('debug', 'var=preconfig_python_version')

    pkg = host.package(name)
    assert pkg.is_installed
    assert pkg.version.startswith(str(result['preconfig_python_version']))
